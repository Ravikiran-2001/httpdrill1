const http = require("http"); //importing http module
const uuid = require("uuid"); //importing uuid module
const server = http.createServer((request, response) => {
  //creating server
  if (request.url === "/html") {
    //if my request url has html
    response.writeHead(200, { "Content-Type": "text/html" }); //setting status code and header for http response
    //write() used to write response
    response.write(` 
    <!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
          <p> - Martin Fowler</p>
      </body>
    </html>
    `);
    response.end(); //response has been fully sent
  }
   else if (request.url === "/json") {
    //if my request url has json
    response.writeHead(200, { "content-Type": "application/json" }); //setting status code and header for http response
    const jsonData = {
      slideshow: {
        author: "Yours Truly",
        date: "date of publication",
        slides: [
          {
            title: "Wake up to WonderWidgets!",
            type: "all",
          },
          {
            items: [
              "Why <em>WonderWidgets</em> are great",
              "Who <em>buys</em> WonderWidgets",
            ],
            title: "Overview",
            type: "all",
          },
        ],
        title: "Sample Slide Show",
      },
    };
    response.write(JSON.stringify(jsonData)); //write() used to write response and converting it to json string
    response.end(); //response has been fully sent
  } 
  else if (request.url === "/uuid") {
    response.writeHead(200, { "Content-Type": "application/json" }); //setting status code and header for http response
    const uuidContent = { uuid: uuid.v4() };
    response.write(JSON.stringify(uuidContent)); //write() used to write response
    response.end(); //response has been fully sent
  } 
  else if (request.url.startsWith("/status/")) {
    //if my url starts with status
    const seperatedArray = request.url.split("/"); //spliting url with respect to '/'
    if (seperatedArray[2] >= 100 && seperatedArray[2] <= 500) {
      response.writeHead(200, { "Content-Type": "text/json" }); //setting status code and header for http response
      response.write(`response with ${seperatedArray[2]} status code`); //write() used to write response
      response.end(); //response has been fully sent
    }
     else {
      response.writeHead(404, { "Content-Type": "text/json" }); //setting status code and header for http response
      response.write(`invalid status port`); //write() used to write response
      response.end(); //response has been fully sent
    }
  } 
  else if (request.url.startsWith("/delay/")) {
    //if my url starts with delay
    const seperatedArray = request.url.split("/"); //spliting url with respect to '/'
    setTimeout(() => {
      response.writeHead(200, { "Content-Type": "text/json" }); //setting status code and header for http response
      response.write(`success`); //write() used to write response
      response.end(); //response has been fully sent
    }, parseInt(seperatedArray[2]) * 1000); //setting timeOut
  } 
  else {
    response.writeHead(404, { "Content-Type": "text/json" });
    response.write(`404 error`); //write() used to write response
    response.end(); //response has been fully sent
  }
});
server.listen(8000, () => {
  console.log("server is running at http://localhost:8000"); //making server to listen incomming connection on port 8000
});
